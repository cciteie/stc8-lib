#include "STC8xxxx.H"
#include "config.h"
#include "GPIO.h"
#include "UART.h"
#include "timer.h"
#include "delay.h"
#include "ADC.h"
#include "Exti.h"
#include "I2C.h"

u8 WakeUpSource;
u8 WakeUpCnt;
void main(){
    ADCClass ADC0;
    UartClass Uart1;
    u8 ADCValue;
    I2CCLASS I2C0;
    // InitGPIO(GPIO_P1,GPIO_Pin_All,GPIO_PullUp);
    // InitUartx(UART1,115200ul,BRT_Timer1,Polity_0,UART1_SW_P30_P31);
    // InitTimer(Timer0,TIM_16BitAutoReload,Polity_0,TIM_CLOCK_1T,100);
    ADC0.InitADC(ADC_SPEED_2X1T);
    Uart1.InitUartx(UART1,115200ul,BRT_Timer1,Polity_0,UART1_SW_P30_P31);
    InitExt(EXT_INT0,EXT_MODE_Fall);
    I2C0.I2CInitMaster(30,I2C_P14_P15);
    // EA=1;
    
        

    while (1)
    {
        // P11=~P11;
        // delay_ms(1);
        ADCValue = ADC0.Get_ADCResult(0);
        ADCValue = ADC0.ADCValueChangeV(ADCValue,5);
        Uart1.Uart1SendChar(0x55);
    }
    
}


